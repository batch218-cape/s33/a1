fetch('https://jsonplaceholder.typicode.com/todos')
.then((response)=>response.json())
.then((json) => {
    const titles = json.map(myFunction)
    function myFunction(item) {
        return item.title;
    }
    console.log(titles);
})

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=>response.json())
.then((json) => { 
    console.log(json)
    console.log(`the item '${json.title}' on the list has a status of ${json.completed}`)
})

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})	
.then((response)=>response.json())
.then((json)=> console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', 
{
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated To Do List',
		status: "Pending",
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure.",
		userId: 1,
	})	
})
.then((response)=>response.json())
.then((json)=> console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "PATCH",
	headers: {
		'Content-type' : 'application/json'
	}, 
	body: JSON.stringify({
		status: "Complete",
        dateCompleted: "07/09/21",
	})	
})
.then((response)=>response.json())
.then((json)=> console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE",
})
.then((response)=>response.json())
.then((json)=> console.log(json));
